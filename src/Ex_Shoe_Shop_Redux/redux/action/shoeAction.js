import { ADD_TO_CART } from "../constants/shoeConstants"

export const addToCartAction=(shoe)=>{
    return {
        type: ADD_TO_CART,
        payload: shoe,
    };
};