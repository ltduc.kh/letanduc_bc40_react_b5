import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoe from './ItemShoe';

 class ListShoe extends Component {
  render() {
    console.log('<ListShoe/>', this.props);

    return (
      <div className="row">
        {this.props.list.map((shoe) => {
          return (
            <ItemShoe  item={shoe} />
          );
        })}
      </div>
    );
  }
}
let mapStateToProps=(state) =>{
  return{
    list: state.shoeReducer.listShoe,
  };
};
export default connect(mapStateToProps)(ListShoe)